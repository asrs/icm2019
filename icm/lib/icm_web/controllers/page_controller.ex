defmodule IcmWeb.PageController do
  use IcmWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end

end
