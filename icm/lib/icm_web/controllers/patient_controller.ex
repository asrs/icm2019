defmodule IcmWeb.PatientController do
  use IcmWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end

  def explain(conn, %{"id" => id}) do
    next = String.to_integer(id) + 1 |> Integer.to_string

    case id do
      "0" -> render(conn, "explain.html", data: %{num: id, type: "timer", next: next})
      "1" -> render(conn, "explain.html", data: %{num: id, type: "rep", next: next})
      _ -> render(conn, "feedback.html")
    end
  end

  def exercise(conn, %{"id" => id}) do
    num = String.to_integer(id) + 1 |> Integer.to_string

    case id do
      "0" -> render(conn, "exercise_timer.html", data: %{num: num, type: "timer", prev: id})
      "1" -> render(conn, "exercise_repetition.html", data: %{num: num, type: "rep", prev: id})
      _ -> render(conn, "feedback.html")
    end
  end

  def pass(conn, _params) do
    render(conn, "pass.html")
  end

  def feedback(conn, _params) do
    render(conn, "feedback.html")
  end

end
