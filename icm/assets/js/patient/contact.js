const contactEl= document.querySelector("section#input input");
const buttonContactEl= document.querySelector("section#input button");
const sendEl= document.querySelector("section#input i");

const changeButton = () => {


	if (!!contactEl.value) {
		sendEl.classList.remove('fa-microphone')
		sendEl.classList.add('fa-paper-plane')
	}
	else {
		sendEl.classList.remove('fa-paper-plane')
		sendEl.classList.add('fa-microphone')
	}
}

const contactAction = () => {
	if (sendEl.classList.contains('fa-paper-plane')) {
		window.location.href = "/patient"
	}
	else if(buttonContactEl.classList.contains("record"))
		buttonContactEl.classList.remove("record")
	else
		buttonContactEl.classList.add("record")
}

if (!!contactEl)
	contactEl.addEventListener("input", changeButton)

if (!!sendEl)
	buttonContactEl.addEventListener("click", contactAction)
