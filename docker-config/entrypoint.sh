#!/bin/sh

while ! pg_isready -q -h $PGHOST -p $PGPORT -U $PGUSER
do
	echo "$(date) - waiting for database to start"
	sleep 2
done

if [[ -z `psql -Atqc "\list $PGDATABASE"` ]]; then
	echo "Database $PGDATABASE does not exist. Creating..."
	createdb -E UTF8 $PGDATABASE -l en_US.UTF-8 -T template0
	/app/bin/icm eval "Icm.ReleaseTask.migrate"
else
	echo "Database $PGDATABASE does exists. Continues..."
fi

/app/bin/icm start
